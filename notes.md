I'll use this file to keep track of reference documents and my own notes as I work through the 57 exercises.

[[_TOC_]]

## Project Organization
* [`use crate::x` vs `use packagename::x`](https://users.rust-lang.org/t/use-crate-x-vs-use-packagename-x/44122)
* Multi-file projects and project structure:
    * https://doc.rust-lang.org/reference/items/modules.html#the-path-attribute
    * https://users.rust-lang.org/t/project-structure-best-practices/37575
    * https://hackernoon.com/including-files-and-deeply-directories-in-rust-q35o3yer
    * https://stackoverflow.com/questions/57756927/rust-modules-confusion-when-there-is-main-rs-and-lib-rs
    * https://doc.rust-lang.org/cargo/guide/project-layout.html
* `main.rs` vs `lib.rs`
    * https://www.reddit.com/r/rust/comments/c41iph/mainrs_vs_librs/
* Import structure / ordering
    * https://stackoverflow.com/questions/45618552/where-is-the-recommended-place-to-put-use-declarations-in-rust


## Basic Commands
* `cargo build`
* `cargo run`
* `cargo fmt`
* `cargo doc (--open)`


## Code style
Rust includes a couple commands to enforce consistent code style. There are two methods for running
the code style commands in rust:
1. `rustfmt <options and files>` will allow you to specify individual files that should be formatted.
2. `cargo fmt <options>` will format all the `*.rs` files in your crate or workspace

You can include a `rustfmt.toml` or `.rustfmt.toml` file in your project to specify the style
guidelines for your project. Even if you plan to use the rustfmt defaults, consider including a
`rustfmt.toml`.

More info:
* https://github.com/rust-lang/rustfmt
* https://doc.rust-lang.org/stable/rust-by-example/testing/unit_testing.html
    * https://stackoverflow.com/questions/28370126/how-can-i-test-stdin-and-stdout
