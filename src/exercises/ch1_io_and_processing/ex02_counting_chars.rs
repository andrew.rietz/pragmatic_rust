/// # Exercise 2
/// Prompt for an input string. Display the string and number of characters
///
/// ## Requirements
/// * [x] Output should contain the original string
/// * [x] Use a single output statement
/// * [x] Use a built-in function of the language to determine string length
/// 
/// ## Challenges
/// * [x] Require the user to enter something into the program
use crate::io_utils;

/// Accept a user string and display the number of characters
pub fn main() {
    let prompt = "Provide an input and I'll tell you how many characters it contains!";
    let mut resp;
    loop {
        resp = io_utils::prompt_and_get_response(prompt, "Invalid input, try again.");
        if resp == "" { 
            println!("Your entry must contain at least one non-whitespace character.");
            continue;
        }
        break;
    }
    println!("The input [{}] contains {} characters.", resp, resp.len())
}
