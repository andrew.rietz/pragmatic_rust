/// # Exercise 1: Take a name as input and print a greeting
///
/// ## Requirements
/// * [x] Keep the input, concatenation, and output separate
///
/// ## Challenges
/// * [ ] Write a version without using any variables
/// * [x] Write a version that displays different greetings
pub mod hello {
    use std::string::String;
    use rand::{thread_rng, Rng};
    use crate::io_utils;

    pub fn main() {
        println!("Hello!");
        loop {
            let name = io_utils::prompt_and_get_response(
                "What's your name?",
                "Please enter a valid name.",
            );
            match is_valid_name(&name) {
                false => println!("Please enter a valid name using only alphabetic characters.\n"),
                true => {
                    println! {"\n{}, {}.", get_random_greeting(), name}
                    break;
                }
            }
        }
    }

    /// Validates that `name` only contain alphabetic characters
    pub fn is_valid_name(name: &str) -> bool {
        let mut is_valid = true;
        for c in name.chars() {
            if !c.is_ascii_alphabetic() {
                is_valid = false;
                break;
            }
        }
        is_valid
    }

    /// A public constant holding potential greetings
    pub const GREETINGS: &[&str] = &[
        "Nice to meet you",
        "It's a pleasure",
        "Hi there",
        "Good day",
    ];

    /// Select a random response from our list of GREETINGS
    pub fn get_random_greeting() -> String {
        let mut rng = thread_rng();
        GREETINGS[rng.gen_range(0, GREETINGS.len())].to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::hello;

    #[test]
    fn is_valid_name() {
        assert!(hello::is_valid_name("Luke"));
        assert!(!hello::is_valid_name("R2D2"));
    }

    #[test]
    fn get_random_greeting() {
        let str_slice = &&hello::get_random_greeting()[..]; // Take a full slice of the String
        assert!(hello::GREETINGS.contains(str_slice))
    }
}
