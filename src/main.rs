use pragmatic_rust::io_utils;

fn main() {
    println!("Hello! Welcome to pragmatic_rust.\n\n");
    let prompt = "  Select an exercise to run:\n
--------------------------------------------
  01: Hello World 
  02: Counting the Number of Characters
  zz: A response option that doesn't exist\n\n

Your selection: ";

    let selection = io_utils::prompt_and_get_response(prompt, "Please select a valid choice.\n");

    // let selection: u32 = selection.parse().unwrap();
    match &&selection[..] {
        &"1" | &"01" => pragmatic_rust::ex01_hello_world::hello::main(),
        &"2" | &"02" => pragmatic_rust::ex02_counting_chars::main(),
        &&_ => (),
    };
}
