use std::io;

/// Convenience wrapper for getting user input from `Stdin`
/// **Not tested.**
///
/// Will revisit in the future.
pub fn prompt_and_get_response(prompt: &str, e_msg: &str) -> String {
    println!("{}", prompt);

    let mut name = String::new();
    io::stdin().read_line(&mut name).expect(e_msg);
    name.trim().to_string()
}
