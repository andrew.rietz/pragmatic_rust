// Utilities
#[path = "utils/io.rs"]
pub mod io_utils;

// Exercises
#[path = "./exercises/ch1_io_and_processing/ex01_hello_world.rs"]
pub mod ex01_hello_world;

#[path = "./exercises/ch1_io_and_processing/ex02_counting_chars.rs"]
pub mod ex02_counting_chars;
