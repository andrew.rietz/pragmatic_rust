# pragmatic_rust
Learning Rust through the Pragmatic Programmers 57 Exercises. Additional details and constraints available in the full text:
>[Pragmatic Programmer - Challenges to Develop Your Coding Skills](https://www.amazon.com/Exercises-Programmers-Challenges-Develop-Coding/dp/1680501224)  

## Project Structure
The project adheres to the following structure:
```sh
.
├── Cargo.toml                      # Package info (deps, modules, etc.)
├── README.md                       # Package overview
└── src                             # Source code
   ├── exercises                    # Exercises library
   │  ├── ch1_io_and_processing     # Each chapter has a subfolder
   │  │  └── ex01_hello_world       # Each exercise has a subfolder
   │  │     ├── hello_world.rs      # One or more files
   │  │     └── README.md           # And a README describing the exercise
   │  ├── ch2_calculations          # Another chapter
   │  │  └── ex...                  # With exercises, etc.
   │  │  └── ex...                  
   │  ├── ...
   │  └── lib.rs                    # All of the exercises are referenced by the lib
   ├── main.rs                      # And the lib is used in the package main.rs
   └── utils                        # Utils are also available for use within the project
      ├── some_util.rs
      ├── some_other_util.rs      
      └── ...
```

## Code Styling
I'm using a `.rustfmt.toml` file to specify the code style parameters for this project.


## Git Commit Template
This project is utilizing a git commit template to ensure commits follow a logical set of guidelines. The template is commited to source control and is enabled with:

```sh
git config --local commit.template .git-commit-template
```
